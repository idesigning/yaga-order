<?php
/**
 * YagaOrderApiInterface
 * PHP version 5
 *
 * @category Class
 * @package  YagaOrder\Server
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga order requests. Swagger
 *
 * OpenAPI spec version: 18.47.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace YagaOrder\Server\Api;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use YagaOrder\Server\Model\BookTicketsResult;
use YagaOrder\Server\Model\CheckPromocodeResult;
use YagaOrder\Server\Model\GetSessionSeatsResult;
use YagaOrder\Server\Model\GetTicketPdfResult;
use YagaOrder\Server\Model\Order;
use YagaOrder\Server\Model\OrderStatusResult;
use YagaOrder\Server\Model\RefundNotify;
use YagaOrder\Server\Model\RequestOrder;
use YagaOrder\Server\Model\ReserveTicketsResult;

/**
 * YagaOrderApiInterface Interface Doc Comment
 *
 * @category Interface
 * @package  YagaOrder\Server\Api
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
interface YagaOrderApiInterface
{

    /**
     * Operation bookTickets
     *
     * @param  YagaOrder\Server\Model\RequestOrder $body  (*) информация о заказе, которая передается в методы работы с заказом. (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\BookTicketsResult[]
     *
     */
    public function bookTickets(RequestOrder $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation cancelPayedReservation
     *
     * @param  YagaOrder\Server\Model\RequestOrder $body  (*) информация о заказе, которая передается в методы работы с заказом. (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\OrderStatusResult[]
     *
     */
    public function cancelPayedReservation(RequestOrder $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation checkPromocode
     *
     * @param  YagaOrder\Server\Model\RequestOrder $body  (*) информация о заказе, которая передается в методы работы с заказом. (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\CheckPromocodeResult[]
     *
     */
    public function checkPromocode(RequestOrder $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation clearReservation
     *
     * @param  YagaOrder\Server\Model\RequestOrder $body  (*) информация о заказе, которая передается в методы работы с заказом. (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\OrderStatusResult[]
     *
     */
    public function clearReservation(RequestOrder $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation getOrder
     *
     * @param  YagaOrder\Server\Model\RequestOrder $body  (*) информация о заказе, которая передается в методы работы с заказом. (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\Order[]
     *
     */
    public function getOrder(RequestOrder $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation getOrderStatus
     *
     * @param  YagaOrder\Server\Model\RequestOrder $body  (*) информация о заказе, которая передается в методы работы с заказом. (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\OrderStatusResult[]
     *
     */
    public function getOrderStatus(RequestOrder $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation getSessionSeats
     *
     * @param  string $sessionId   (optional)
     * @param  string $venueId   (optional)
     * @param  string $eventId   (optional)
     * @param  string $hallId   (optional)
     * @param  \DateTime $sessionTime   (optional)
     * @param  string[] $additional   (optional)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\GetSessionSeatsResult[]
     *
     */
    public function getSessionSeats($sessionId = null, $venueId = null, $eventId = null, $hallId = null, \DateTime $sessionTime = null, array $additional = null, &$responseCode, array &$responseHeaders);

    /**
     * Operation getTicketPdf
     *
     * @param  string $ticketId  (*) идентификатор билета (required)
     * @param  YagaOrder\Server\Model\RequestOrder $body  (*) информация о заказе, которая передается в методы работы с заказом. (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\GetTicketPdfResult[]
     *
     */
    public function getTicketPdf($ticketId, RequestOrder $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation refundNotify
     *
     * @param  YagaOrder\Server\Model\RefundNotify $body   (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\OrderStatusResult[]
     *
     */
    public function refundNotify(RefundNotify $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation reserveTickets
     *
     * @param  YagaOrder\Server\Model\RequestOrder $body  (*) информация о заказе, которая передается в методы работы с заказом. (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\ReserveTicketsResult[]
     *
     */
    public function reserveTickets(RequestOrder $body, &$responseCode, array &$responseHeaders);

    /**
     * Operation saleApprove
     *
     * @param  YagaOrder\Server\Model\RequestOrder $body  (*) информация о заказе, которая передается в методы работы с заказом. (required)
     * @param  integer $responseCode     The HTTP response code to return
     * @param  array   $responseHeaders  Additional HTTP headers to return with the response ()
     *
     * @return \YagaOrder\Server\Model\OrderStatusResult[]
     *
     */
    public function saleApprove(RequestOrder $body, &$responseCode, array &$responseHeaders);
}
