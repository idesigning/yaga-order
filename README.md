# SwaggerServer
Common schema for Yaga order requests. Swagger

This [Symfony](https://symfony.com/) bundle is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 18.47.0
- Build package: io.swagger.codegen.languages.SymfonyServerCodegen

## Requirements

PHP 5.4.0 and later

## Installation & Usage

To install the dependencies via [Composer](http://getcomposer.org/), add the following repository to `composer.json` of your Symfony project:

```json
{
    "repositories": [{
        "type": "path",
        "url": "//Path to your generated swagger bundle"
    }],
}
```

Then run:

```
composer require swagger/server-bundle:dev-master
```

to add the generated swagger bundle as a dependency.

## Tests

To run the unit tests for the generated bundle, first navigate to the directory containing the code, then run the following commands:

```
composer install
./vendor/bin/phpunit
```


## Getting Started

Step 1: Please follow the [installation procedure](#installation--usage) first.

Step 2: Enable the bundle in the kernel:

```php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new YagaOrder\Server\SwaggerServerBundle(),
        // ...
    );
}
```

Step 3: Register the routes:

```yaml
# app/config/routing.yml
swagger_server:
    resource: "@SwaggerServerBundle/Resources/config/routing.yml"
```

Step 4: Implement the API calls:

```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface // An interface is autogenerated
{

    // Other operation methods ...
}
```

Step 5: Tag your API implementation:

```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.yagaOrder:
        class: Acme\MyBundle\Api\YagaOrderApi
        tags:
            - { name: "swagger_server.api", api: "yagaOrder" }
    # ...
```

Now you can start using the bundle!


## Documentation for API Endpoints

All URIs are relative to *https://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*YagaOrderApiInterface* | [**bookTickets**](Resources/docs/Api/YagaOrderApiInterface.md#booktickets) | **POST** /book | 
*YagaOrderApiInterface* | [**cancelPayedReservation**](Resources/docs/Api/YagaOrderApiInterface.md#cancelpayedreservation) | **POST** /cancel-order | 
*YagaOrderApiInterface* | [**checkPromocode**](Resources/docs/Api/YagaOrderApiInterface.md#checkpromocode) | **POST** /check-promocode | 
*YagaOrderApiInterface* | [**clearReservation**](Resources/docs/Api/YagaOrderApiInterface.md#clearreservation) | **POST** /clear-reservation | 
*YagaOrderApiInterface* | [**getOrder**](Resources/docs/Api/YagaOrderApiInterface.md#getorder) | **POST** /order-info | 
*YagaOrderApiInterface* | [**getOrderStatus**](Resources/docs/Api/YagaOrderApiInterface.md#getorderstatus) | **POST** /order-status | 
*YagaOrderApiInterface* | [**getSessionSeats**](Resources/docs/Api/YagaOrderApiInterface.md#getsessionseats) | **GET** /available-seats | 
*YagaOrderApiInterface* | [**getTicketPdf**](Resources/docs/Api/YagaOrderApiInterface.md#getticketpdf) | **POST** /pdf/{ticketId} | 
*YagaOrderApiInterface* | [**refundNotify**](Resources/docs/Api/YagaOrderApiInterface.md#refundnotify) | **POST** /refund-notify | 
*YagaOrderApiInterface* | [**reserveTickets**](Resources/docs/Api/YagaOrderApiInterface.md#reservetickets) | **POST** /reserve | 
*YagaOrderApiInterface* | [**saleApprove**](Resources/docs/Api/YagaOrderApiInterface.md#saleapprove) | **POST** /approve | 


## Documentation For Models

 - [Addition](Resources/docs/Model/Addition.md)
 - [AdditionLimits](Resources/docs/Model/AdditionLimits.md)
 - [AdmissionState](Resources/docs/Model/AdmissionState.md)
 - [AttachmentType](Resources/docs/Model/AttachmentType.md)
 - [BarcodeType](Resources/docs/Model/BarcodeType.md)
 - [BookTicketsResult](Resources/docs/Model/BookTicketsResult.md)
 - [CheckPromocodeResult](Resources/docs/Model/CheckPromocodeResult.md)
 - [CheckPromocodeStatus](Resources/docs/Model/CheckPromocodeStatus.md)
 - [Cost](Resources/docs/Model/Cost.md)
 - [Customer](Resources/docs/Model/Customer.md)
 - [Delivery](Resources/docs/Model/Delivery.md)
 - [DeliveryType](Resources/docs/Model/DeliveryType.md)
 - [GetSessionSeatsResult](Resources/docs/Model/GetSessionSeatsResult.md)
 - [GetTicketPdfResult](Resources/docs/Model/GetTicketPdfResult.md)
 - [LevelState](Resources/docs/Model/LevelState.md)
 - [Money](Resources/docs/Model/Money.md)
 - [Order](Resources/docs/Model/Order.md)
 - [OrderItem](Resources/docs/Model/OrderItem.md)
 - [OrderStatus](Resources/docs/Model/OrderStatus.md)
 - [OrderStatusResult](Resources/docs/Model/OrderStatusResult.md)
 - [PaymentType](Resources/docs/Model/PaymentType.md)
 - [PromocodeInfo](Resources/docs/Model/PromocodeInfo.md)
 - [RefundNotify](Resources/docs/Model/RefundNotify.md)
 - [RequestOrder](Resources/docs/Model/RequestOrder.md)
 - [ReserveTicketsResult](Resources/docs/Model/ReserveTicketsResult.md)
 - [Seat](Resources/docs/Model/Seat.md)
 - [SeatCategory](Resources/docs/Model/SeatCategory.md)
 - [SeatCategoryCostChange](Resources/docs/Model/SeatCategoryCostChange.md)
 - [SeatCategoryDiscount](Resources/docs/Model/SeatCategoryDiscount.md)
 - [SeatState](Resources/docs/Model/SeatState.md)
 - [SessionTime](Resources/docs/Model/SessionTime.md)
 - [SessionType](Resources/docs/Model/SessionType.md)
 - [SoldTicket](Resources/docs/Model/SoldTicket.md)
 - [SupplementType](Resources/docs/Model/SupplementType.md)
 - [Vat](Resources/docs/Model/Vat.md)


## Documentation For Authorization

 All endpoints do not require authorization.


## Author




