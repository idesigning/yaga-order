<?php
/**
 * SeatCategory
 *
 * PHP version 5
 *
 * @category Class
 * @package  YagaOrder\Server\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga order requests. Swagger
 *
 * OpenAPI spec version: 18.47.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace YagaOrder\Server\Model;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class representing the SeatCategory model.
 *
 * Категория места.  Характеризует стоимость конкретного места и возможные дополнения, которые можно купить вместе с местом (например, 3D-очки для кино).
 *
 * @package YagaOrder\Server\Model
 * @author  Swagger Codegen team
 */
class SeatCategory 
{
        /**
     * (*) идентификатор категории
     *
     * @var string|null
     * @SerializedName("id")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $id;

    /**
     * (*) название категории
     *
     * @var string|null
     * @SerializedName("name")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $name;

    /**
     * (*) стоимость
     *
     * @var \YagaOrder\Server\Model\Cost|null
     * @SerializedName("cost")
     * @Assert\Type("YagaOrder\Server\Model\Cost")
     * @Type("YagaOrder\Server\Model\Cost")
     */
    protected $cost;

    /**
     * доступные дополнения
     *
     * @var \YagaOrder\Server\Model\Addition[]|null
     * @SerializedName("availableAdditions")
     * @Assert\All({
     *   @Assert\Type("YagaOrder\Server\Model\Addition")
     * })
     * @Type("array<YagaOrder\Server\Model\Addition>")
     */
    protected $availableAdditions;

    /**
     * (*?) лимиты на покупку доступных дополнений. Обязательно при наличии доступных дополнений.
     *
     * @var \YagaOrder\Server\Model\AdditionLimits[]|null
     * @SerializedName("additionLimits")
     * @Assert\All({
     *   @Assert\Type("YagaOrder\Server\Model\AdditionLimits")
     * })
     * @Type("array<YagaOrder\Server\Model\AdditionLimits>")
     */
    protected $additionLimits;

    /**
     * скидка, учтенная в стоимости
     *
     * @var \YagaOrder\Server\Model\SeatCategoryDiscount|null
     * @SerializedName("discount")
     * @Assert\Type("YagaOrder\Server\Model\SeatCategoryDiscount")
     * @Type("YagaOrder\Server\Model\SeatCategoryDiscount")
     */
    protected $discount;

    /**
     * ожидаемые изменения стоимости
     *
     * @var \YagaOrder\Server\Model\SeatCategoryCostChange[]|null
     * @SerializedName("costChanges")
     * @Assert\All({
     *   @Assert\Type("YagaOrder\Server\Model\SeatCategoryCostChange")
     * })
     * @Type("array<YagaOrder\Server\Model\SeatCategoryCostChange>")
     */
    protected $costChanges;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->name = isset($data['name']) ? $data['name'] : null;
        $this->cost = isset($data['cost']) ? $data['cost'] : null;
        $this->availableAdditions = isset($data['availableAdditions']) ? $data['availableAdditions'] : null;
        $this->additionLimits = isset($data['additionLimits']) ? $data['additionLimits'] : null;
        $this->discount = isset($data['discount']) ? $data['discount'] : null;
        $this->costChanges = isset($data['costChanges']) ? $data['costChanges'] : null;
    }

    /**
     * Gets id.
     *
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets id.
     *
     * @param string|null $id  (*) идентификатор категории
     *
     * @return $this
     */
    public function setId($id = null)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets name.
     *
     * @param string|null $name  (*) название категории
     *
     * @return $this
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets cost.
     *
     * @return \YagaOrder\Server\Model\Cost|null
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Sets cost.
     *
     * @param \YagaOrder\Server\Model\Cost|null $cost  (*) стоимость
     *
     * @return $this
     */
    public function setCost(Cost $cost = null)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Gets availableAdditions.
     *
     * @return \YagaOrder\Server\Model\Addition[]|null
     */
    public function getAvailableAdditions()
    {
        return $this->availableAdditions;
    }

    /**
     * Sets availableAdditions.
     *
     * @param \YagaOrder\Server\Model\Addition[]|null $availableAdditions  доступные дополнения
     *
     * @return $this
     */
    public function setAvailableAdditions(Addition $availableAdditions = null)
    {
        $this->availableAdditions = $availableAdditions;

        return $this;
    }

    /**
     * Gets additionLimits.
     *
     * @return \YagaOrder\Server\Model\AdditionLimits[]|null
     */
    public function getAdditionLimits()
    {
        return $this->additionLimits;
    }

    /**
     * Sets additionLimits.
     *
     * @param \YagaOrder\Server\Model\AdditionLimits[]|null $additionLimits  (*?) лимиты на покупку доступных дополнений. Обязательно при наличии доступных дополнений.
     *
     * @return $this
     */
    public function setAdditionLimits(AdditionLimits $additionLimits = null)
    {
        $this->additionLimits = $additionLimits;

        return $this;
    }

    /**
     * Gets discount.
     *
     * @return \YagaOrder\Server\Model\SeatCategoryDiscount|null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Sets discount.
     *
     * @param \YagaOrder\Server\Model\SeatCategoryDiscount|null $discount  скидка, учтенная в стоимости
     *
     * @return $this
     */
    public function setDiscount(SeatCategoryDiscount $discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Gets costChanges.
     *
     * @return \YagaOrder\Server\Model\SeatCategoryCostChange[]|null
     */
    public function getCostChanges()
    {
        return $this->costChanges;
    }

    /**
     * Sets costChanges.
     *
     * @param \YagaOrder\Server\Model\SeatCategoryCostChange[]|null $costChanges  ожидаемые изменения стоимости
     *
     * @return $this
     */
    public function setCostChanges(SeatCategoryCostChange $costChanges = null)
    {
        $this->costChanges = $costChanges;

        return $this;
    }
}


