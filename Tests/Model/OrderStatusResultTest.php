<?php
/**
 * OrderStatusResultTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  YagaOrder\Server\Tests\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga order requests. Swagger
 *
 * OpenAPI spec version: 18.47.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace YagaOrder\Server\Model;

/**
 * OrderStatusResultTest Class Doc Comment
 *
 * @category    Class */
// * @description Ответ на запрос текущего статуса заказа
/**
 * @package     YagaOrder\Server\Tests\Model
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderStatusResultTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "OrderStatusResult"
     */
    public function testOrderStatusResult()
    {
        $testOrderStatusResult = new OrderStatusResult();
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "orderNumber"
     */
    public function testPropertyOrderNumber()
    {
    }

    /**
     * Test attribute "specificFields"
     */
    public function testPropertySpecificFields()
    {
    }

    /**
     * Test attribute "additional"
     */
    public function testPropertyAdditional()
    {
    }
}
