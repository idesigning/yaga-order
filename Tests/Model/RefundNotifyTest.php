<?php
/**
 * RefundNotifyTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  YagaOrder\Server\Tests\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga order requests. Swagger
 *
 * OpenAPI spec version: 18.47.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace YagaOrder\Server\Model;

/**
 * RefundNotifyTest Class Doc Comment
 *
 * @category    Class */
// * @description Нотификация о проведении возврата денег за билет.
/**
 * @package     YagaOrder\Server\Tests\Model
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class RefundNotifyTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "RefundNotify"
     */
    public function testRefundNotify()
    {
        $testRefundNotify = new RefundNotify();
    }

    /**
     * Test attribute "order"
     */
    public function testPropertyOrder()
    {
    }

    /**
     * Test attribute "notifyAmount"
     */
    public function testPropertyNotifyAmount()
    {
    }
}
