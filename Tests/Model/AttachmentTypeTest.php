<?php
/**
 * AttachmentTypeTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  YagaOrder\Server\Tests\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga order requests. Swagger
 *
 * OpenAPI spec version: 18.47.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace YagaOrder\Server\Model;

/**
 * AttachmentTypeTest Class Doc Comment
 *
 * @category    Class */
// * @description Тип вложения  Влияет на то, как создается бланк, который после покупки высылается пользователю.   - UNDEFINED_ATTACHMENT_TYPE: Не определен. НЕ ИСПОЛЬЗВАТЬ.  - NONE: Нет вложений.  - ORDER: Бланк генерится на заказ целиком.  - TICKETS: Бланк генерится на каждый билет.
/**
 * @package     YagaOrder\Server\Tests\Model
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AttachmentTypeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "AttachmentType"
     */
    public function testAttachmentType()
    {
        $testAttachmentType = new AttachmentType();
    }
}
