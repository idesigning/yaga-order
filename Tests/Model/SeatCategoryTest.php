<?php
/**
 * SeatCategoryTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  YagaOrder\Server\Tests\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga order requests. Swagger
 *
 * OpenAPI spec version: 18.47.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace YagaOrder\Server\Model;

/**
 * SeatCategoryTest Class Doc Comment
 *
 * @category    Class */
// * @description Категория места.  Характеризует стоимость конкретного места и возможные дополнения, которые можно купить вместе с местом (например, 3D-очки для кино).
/**
 * @package     YagaOrder\Server\Tests\Model
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SeatCategoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "SeatCategory"
     */
    public function testSeatCategory()
    {
        $testSeatCategory = new SeatCategory();
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "cost"
     */
    public function testPropertyCost()
    {
    }

    /**
     * Test attribute "availableAdditions"
     */
    public function testPropertyAvailableAdditions()
    {
    }

    /**
     * Test attribute "additionLimits"
     */
    public function testPropertyAdditionLimits()
    {
    }

    /**
     * Test attribute "discount"
     */
    public function testPropertyDiscount()
    {
    }

    /**
     * Test attribute "costChanges"
     */
    public function testPropertyCostChanges()
    {
    }
}
