# OrderStatusResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | идентификатор заказа | [optional] 
**status** | [**YagaOrder\Server\Model\OrderStatus**](OrderStatus.md) | (*) статус заказа | [optional] 
**orderNumber** | **string** | (*?) номер заказа у партнера. Поле заполняется после успешного бронирования билетов (YagaOrderService.ReserveTickets -&gt; order_number) или после успешного подтверждения покупки (YagaOrderService.SaleApprove -&gt; order_number) или после успешнего создания долгосрочной брони (YagaOrderService.BookTickets -&gt; order_number) | [optional] 
**specificFields** | **string** | служебное поле | [optional] 
**additional** | **array** | служебное поле | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


