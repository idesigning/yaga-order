# GetSessionSeatsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seatCategories** | [**YagaOrder\Server\Model\SeatCategory**](SeatCategory.md) | (*) список доступных категорий | [optional] 
**gatewayLevelStates** | [**YagaOrder\Server\Model\LevelState**](LevelState.md) | (*) список уровней с информацией о доступных для продажи местах | [optional] 
**deliveries** | [**YagaOrder\Server\Model\Delivery**](Delivery.md) | список возможных способов покупки билета | [optional] 
**additional** | **array** | служебное поле | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


