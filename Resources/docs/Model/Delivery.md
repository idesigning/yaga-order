# Delivery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**YagaOrder\Server\Model\DeliveryType**](DeliveryType.md) | (*) тип доставки билета | [optional] 
**paymentTypes** | [**YagaOrder\Server\Model\PaymentType**](PaymentType.md) | (*) способы оплаты билета | [optional] 
**price** | [**YagaOrder\Server\Model\Money**](Money.md) | стоимость доставки | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


