# SessionTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionStart** | [**\DateTime**](\DateTime.md) | (*) время начала | [optional] 
**sessionEnd** | [**\DateTime**](\DateTime.md) | время конца | [optional] 
**type** | [**YagaOrder\Server\Model\SessionType**](SessionType.md) | тип | [optional] 
**timezone** | **string** | (*) идентификатор таймзоны по базе https://www.iana.org/time-zones (например, Europe/Moscow) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


