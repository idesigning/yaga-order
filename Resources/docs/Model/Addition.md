# Addition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*) идентификатор дополнения | [optional] 
**type** | [**YagaOrder\Server\Model\SupplementType**](SupplementType.md) | (*) тип дополнения | [optional] 
**cost** | [**YagaOrder\Server\Model\Cost**](Cost.md) | (*) стоимость дополнения | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


