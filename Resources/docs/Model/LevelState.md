# LevelState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*) идентификатор уровня. Он должен быть согласован с идентификатором уровня (Level) в зале (Hall), который приходит при импорте залов, и с идентификатором уровня билета в заказе (SoldTicket.level_id) | [optional] 
**name** | **string** | (*) название уровня | [optional] 
**admission** | **bool** | (*) true для уровня без мест, false для уровня с местами (по-умолчанию false) | [optional] 
**seatStates** | [**YagaOrder\Server\Model\SeatState**](SeatState.md) | (*?) список состояний мест в уровне с местами. В этом списке не обязательно должны быть все места увроня, достаточно только доступных для продажи мест. Обязательно для уровня с местами. | [optional] 
**admissionStates** | [**YagaOrder\Server\Model\AdmissionState**](AdmissionState.md) | (*?) список состояний уровня без мест. Обязательно для уровня без мест. | [optional] 
**entranceId** | **string** | идентификатор входа (например подъезда в Олимпийском) | [optional] 
**entranceName** | **string** | название входа (например подъезда в Олимпийском) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


