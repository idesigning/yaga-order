# Money

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **string** | (*) значение в минимальной единице валюты (для рубля - копейки, для доллара - центы и т.д.) | [optional] 
**currencyCode** | **string** | (*) код валюты по ISO 4217 (RUB, USD, EUR и т.д.) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


