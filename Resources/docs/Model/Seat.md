# Seat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*) идентификатор места | [optional] 
**row** | **string** | (*) номер ряда | [optional] 
**place** | **string** | (*) номер места | [optional] 
**fragment** | **string** | идентификатор фрагмента. Места, объединенные одним фрагментом, нельзя купить по отдельности. | [optional] 
**x** | **int** | координата X на плане зала | [optional] 
**y** | **int** | координата Y на плане зала | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


