# SeatCategoryCostChange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newCost** | [**YagaOrder\Server\Model\Cost**](Cost.md) | (*) новая стоимость | [optional] 
**startDate** | [**\DateTime**](\DateTime.md) | (*) дата старта действия новой стоимости | [optional] 
**endDate** | [**\DateTime**](\DateTime.md) | дата окончания действия новой стоимости | [optional] 
**description** | **string** | опциональное описание | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


