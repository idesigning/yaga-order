# SeatCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*) идентификатор категории | [optional] 
**name** | **string** | (*) название категории | [optional] 
**cost** | [**YagaOrder\Server\Model\Cost**](Cost.md) | (*) стоимость | [optional] 
**availableAdditions** | [**YagaOrder\Server\Model\Addition**](Addition.md) | доступные дополнения | [optional] 
**additionLimits** | [**YagaOrder\Server\Model\AdditionLimits**](AdditionLimits.md) | (*?) лимиты на покупку доступных дополнений. Обязательно при наличии доступных дополнений. | [optional] 
**discount** | [**YagaOrder\Server\Model\SeatCategoryDiscount**](SeatCategoryDiscount.md) | скидка, учтенная в стоимости | [optional] 
**costChanges** | [**YagaOrder\Server\Model\SeatCategoryCostChange**](SeatCategoryCostChange.md) | ожидаемые изменения стоимости | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


