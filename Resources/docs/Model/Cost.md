# Cost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | [**YagaOrder\Server\Model\Money**](Money.md) | (*) номинал | [optional] 
**fee** | [**YagaOrder\Server\Model\Money**](Money.md) | (*) сервисный сбор | [optional] 
**total** | [**YagaOrder\Server\Model\Money**](Money.md) | (*) общая стоимость | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


