# AdditionLimits

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**min** | **int** | (*) минимальное число | [optional] 
**max** | **int** | (*) максимальное число | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


