# OrderItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seatId** | **string** | (*?) идентификатор выбранного места (SeatState). Обязателен для увроня с местами. | [optional] 
**levelId** | **string** | (*) идентификатор увроная (LevelState) | [optional] 
**categoryId** | **string** | (*?) идентификатор ценовой категории (SeatCategory). Обязателен для уровня без мест. | [optional] 
**admission** | **bool** | (*) true для уровня без мест, false для уровня с местами (по-умолчанию false) | [optional] 
**cost** | [**YagaOrder\Server\Model\Cost**](Cost.md) | (*) стоимость | [optional] 
**seat** | [**YagaOrder\Server\Model\Seat**](Seat.md) | (*?) информация о месте. Обязателен для увроня с местами (admission &#x3D; false). | [optional] 
**supplementType** | [**YagaOrder\Server\Model\SupplementType**](SupplementType.md) | (*) тип дополнения. Если билет не является дополнением - UNDEFINED_SUPPLEMENT_TYPE. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


