# BookTicketsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*) идентификатор созданного заказа | [optional] 
**status** | [**YagaOrder\Server\Model\OrderStatus**](OrderStatus.md) | (*) статус созданного заказа | [optional] 
**sum** | [**YagaOrder\Server\Model\Cost**](Cost.md) | (*) стоимость забронированных билетов | [optional] 
**orderNumber** | **string** | номер заказа у партнера. | [optional] 
**sessionTime** | [**YagaOrder\Server\Model\SessionTime**](SessionTime.md) | дата/время события (сеанса) | [optional] 
**codeWord** | **string** | кодовое слово (используется некоторыми кинотеатрами) | [optional] 
**timeout** | **string** | длительность брони в секундах, в REST оканчивающая на s, например \&quot;259200s\&quot; | [optional] 
**specificFields** | **string** | служебное поле | [optional] 
**additional** | **array** | служебное поле | [optional] 
**barcode** | **string** | баркод, который нужно напечатать на бланке заказа после покупки. Например, если тип вложения будет ORDER и баркод должен отличаться от order_number. Для заказов с типом вложения TICKETS и NONE это поле заполнять нет смысла. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


