# SoldTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*) идентификатор билета | [optional] 
**barcode** | **string** | (*?) баркод. Обязателен для заказа в статусе APPROVED. | [optional] 
**admission** | **bool** | (*) true для уровня без мест, false для уровня с местами (по-умолчанию false) | [optional] 
**levelId** | **string** | (*) идентификатор уровня (LevelState) | [optional] 
**levelName** | **string** | (*) название уровня (LevelState) | [optional] 
**categoryId** | **string** | (*?) идентификатор ценовой категории. Обязателен для уровня без мест. | [optional] 
**categoryName** | **string** | (*?) название ценовой категории. Обязателен для уровня без мест. | [optional] 
**row** | **string** | (*?) ряд. Обязателен для уровня с местами. | [optional] 
**place** | **string** | (*?) место. Обязателен для уровня с местами. | [optional] 
**pdfUrl** | **string** | ссылка, по которой можно скачать партнерский бланк билета в формате pdf. | [optional] 
**barcodeType** | [**YagaOrder\Server\Model\BarcodeType**](BarcodeType.md) | тип баркода | [optional] 
**organizerInfo** | **string** | информация об организаторе события. Обязателен для заказа в статусе APPROVED. | [optional] 
**vat** | [**YagaOrder\Server\Model\Vat**](Vat.md) | тип НДС | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


