# PromocodeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promocodeAmount** | [**YagaOrder\Server\Model\Money**](Money.md) | Номинал промокода, если промокод на номинал. | [optional] 
**promocodePercent** | **double** | Процент промокода, если промокод на процент. | [optional] 
**promocodeDescription** | **string** | Опциональное описание промокода | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


