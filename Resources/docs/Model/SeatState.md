# SeatState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*) идентификатор места | [optional] 
**available** | **bool** | (*) доступно ли место для продажи | [optional] 
**categoryId** | **string** | (*) идентификатор категории места | [optional] 
**seat** | [**YagaOrder\Server\Model\Seat**](Seat.md) | (*) информация о месте | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


