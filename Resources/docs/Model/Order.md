# Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*) идентификатор заказа | [optional] 
**status** | [**YagaOrder\Server\Model\OrderStatus**](OrderStatus.md) | (*) статус заказа | [optional] 
**sum** | [**YagaOrder\Server\Model\Cost**](Cost.md) | (*) суммарная стоимость заказа | [optional] 
**orderNumber** | **string** | (*?) номер заказа. Обязателен для подтвержденных заказов. | [optional] 
**sessionTime** | [**YagaOrder\Server\Model\SessionTime**](SessionTime.md) | время сеанса | [optional] 
**codeWord** | **string** | кодовое слово (используется некоторыми кинотеатрами) | [optional] 
**tickets** | [**YagaOrder\Server\Model\SoldTicket**](SoldTicket.md) | (*?) список билетов в заказе. Обязателен для подтвержденных заказов с типом вложения TICKETS. | [optional] 
**attachmentType** | [**YagaOrder\Server\Model\AttachmentType**](AttachmentType.md) | (*) тип, характеризующий сколько бланков (вложений) нужно печатать на заказ | [optional] 
**partnerComment** | **string** | произвольный комментарий партнера | [optional] 
**organizerInfo** | **string** | (*?) информация об организаторе. Обязателен для заказов с типом вложения ORDER. | [optional] 
**specificFields** | **string** | служебное поле | [optional] 
**additional** | **array** | служебное поле | [optional] 
**barcode** | **string** | баркод, который нужно напечатать на бланке заказа. Например, если тип вложения ORDER и баркод должен отличаться от order_number. Для заказов с типом вложения TICKETS и NONE это поле заполнять нет смысла. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


