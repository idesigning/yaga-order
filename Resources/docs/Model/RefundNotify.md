# RefundNotify

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | [**YagaOrder\Server\Model\RequestOrder**](RequestOrder.md) | заказ, по которому произошел возврат | [optional] 
**notifyAmount** | [**YagaOrder\Server\Model\Money**](Money.md) | сумма возврата | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


