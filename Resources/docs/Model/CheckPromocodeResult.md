# CheckPromocodeResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**YagaOrder\Server\Model\CheckPromocodeStatus**](CheckPromocodeStatus.md) | (*) статус проверки промокода | [optional] 
**statusDescription** | **string** | Опциональное описание статуса | [optional] 
**promocodeInfo** | [**YagaOrder\Server\Model\PromocodeInfo**](PromocodeInfo.md) | (*?) Информация о промокоде. Обязательна, если промокод найден. | [optional] 
**promocodeCost** | [**YagaOrder\Server\Model\Cost**](Cost.md) | (*?) Стоимость, которая была погашена промокодом. Обязательна, если промокод найден и его можно применить. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


