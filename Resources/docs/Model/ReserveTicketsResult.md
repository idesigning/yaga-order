# ReserveTicketsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*) идентификатор созданного заказа | [optional] 
**status** | [**YagaOrder\Server\Model\OrderStatus**](OrderStatus.md) | (*) статус созданного заказа | [optional] 
**sum** | [**YagaOrder\Server\Model\Cost**](Cost.md) | (*) стоимость забронированных билетов | [optional] 
**orderNumber** | **string** | номер заказа у партнера. | [optional] 
**sessionTime** | [**YagaOrder\Server\Model\SessionTime**](SessionTime.md) | дата/время события (сеанса) | [optional] 
**codeWord** | **string** | кодовое слово (используется некоторыми кинотеатрами) | [optional] 
**specificFields** | **string** | служебное поле | [optional] 
**additional** | **array** | служебное поле | [optional] 
**barcode** | **string** | баркод, который нужно напечатать на бланке заказа после покупки. Например, если тип вложения будет ORDER и баркод должен отличаться от order_number. Для заказов с типом вложения TICKETS и NONE это поле заполнять нет смысла. | [optional] 
**timeout** | **string** | длительность брони в секундах, в REST оканчивающая на s, например \&quot;259200s\&quot; | [optional] 
**promocodeCost** | [**YagaOrder\Server\Model\Cost**](Cost.md) | (*?) Стоимость, которая была погашена промокодом. Обязательна, если промокод успешно применен. | [optional] 
**promocodeInfo** | [**YagaOrder\Server\Model\PromocodeInfo**](PromocodeInfo.md) | (*?) Информация о промокоде, если был успешно применен промокод. Обязателен, если промокод успешно применен. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


