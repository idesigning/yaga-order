# RequestOrder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | (*?) идентификатор заказа у партнера. Поле заполняется после успешного бронирования билетов (YagaOrderService.ReserveTickets -&gt; id) или после успешнего создания долгосрочной брони (YagaOrderService.BookTickets -&gt; id) | [optional] 
**sessionId** | **string** | (*) идентификатор сеанса (Session) | [optional] 
**venueId** | **string** | (*) идентификатор площадки (Venue) | [optional] 
**eventId** | **string** | (*) идентификатор события (Event) | [optional] 
**hallId** | **string** | (*) идентификатор зала (Hall) | [optional] 
**sessionTime** | [**YagaOrder\Server\Model\SessionTime**](SessionTime.md) | (*) время сеанса | [optional] 
**created** | [**\DateTime**](\DateTime.md) | (*) дата создания заказа | [optional] 
**reservationTimeout** | **string** | длительность брони, в REST оканчивающая на s, например \&quot;259200s\&quot; | [optional] 
**items** | [**YagaOrder\Server\Model\OrderItem**](OrderItem.md) | (*) список выбранных мест/билетов | [optional] 
**customer** | [**YagaOrder\Server\Model\Customer**](Customer.md) | (*) информация о покупателе | [optional] 
**orderNumber** | **string** | (*?) номер заказа у партнера. Поле заполняется после успешного бронирования билетов (YagaOrderService.ReserveTickets -&gt; order_number) или после успешного подтверждения покупки (YagaOrderService.SaleApprove -&gt; order_number) или после успешнего создания долгосрочной брони (YagaOrderService.BookTickets -&gt; order_number) | [optional] 
**codeWord** | **string** | кодовое слово. Используется некоторыми кинотеатрами. | [optional] 
**sum** | [**YagaOrder\Server\Model\Cost**](Cost.md) | стоимость заказа | [optional] 
**paymentType** | [**YagaOrder\Server\Model\PaymentType**](PaymentType.md) | (*) спокоб оплаты | [optional] 
**deliveryType** | [**YagaOrder\Server\Model\DeliveryType**](DeliveryType.md) | способ доставки | [optional] 
**specificFields** | **string** | (*) произвольные служебные поля, которые заполняются    после успешного бронирования билетов (YagaOrderService.ReserveTickets -&gt; specific_fields)    или после успешного подтверждения покупки (YagaOrderService.SaleApprove -&gt; specific_fields)    или после успешнего создания долгосрочной брони (YagaOrderService.BookTickets -&gt; specific_fields) | [optional] 
**additional** | **array** | (*) служебное поле | [optional] 
**promocode** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


