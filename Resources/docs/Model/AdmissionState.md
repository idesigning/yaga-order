# AdmissionState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryId** | **string** | (*) идентификатор категории (SeatCategory) | [optional] 
**availableSeatCount** | **int** | (*) число доступных для продажи мест | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


