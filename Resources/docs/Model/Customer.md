# Customer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **string** | имя | [optional] 
**lastName** | **string** | фамилия | [optional] 
**email** | **string** | электронная почта | [optional] 
**phone** | **string** | телефон | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


