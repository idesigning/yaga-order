# YagaOrder\Server\Api\YagaOrderApiInterface

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bookTickets**](YagaOrderApiInterface.md#bookTickets) | **POST** /book | 
[**cancelPayedReservation**](YagaOrderApiInterface.md#cancelPayedReservation) | **POST** /cancel-order | 
[**checkPromocode**](YagaOrderApiInterface.md#checkPromocode) | **POST** /check-promocode | 
[**clearReservation**](YagaOrderApiInterface.md#clearReservation) | **POST** /clear-reservation | 
[**getOrder**](YagaOrderApiInterface.md#getOrder) | **POST** /order-info | 
[**getOrderStatus**](YagaOrderApiInterface.md#getOrderStatus) | **POST** /order-status | 
[**getSessionSeats**](YagaOrderApiInterface.md#getSessionSeats) | **GET** /available-seats | 
[**getTicketPdf**](YagaOrderApiInterface.md#getTicketPdf) | **POST** /pdf/{ticketId} | 
[**refundNotify**](YagaOrderApiInterface.md#refundNotify) | **POST** /refund-notify | 
[**reserveTickets**](YagaOrderApiInterface.md#reserveTickets) | **POST** /reserve | 
[**saleApprove**](YagaOrderApiInterface.md#saleApprove) | **POST** /approve | 


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.yagaOrder:
        class: Acme\MyBundle\Api\YagaOrderApi
        tags:
            - { name: "swagger_server.api", api: "yagaOrder" }
    # ...
```

## **bookTickets**
> YagaOrder\Server\Model\BookTicketsResult bookTickets($body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#bookTickets
     */
    public function bookTickets(RequestOrder $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YagaOrder\Server\Model\RequestOrder**](../Model/RequestOrder.md)| (*) информация о заказе, которая передается в методы работы с заказом. |

### Return type

[**YagaOrder\Server\Model\BookTicketsResult**](../Model/BookTicketsResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **cancelPayedReservation**
> YagaOrder\Server\Model\OrderStatusResult cancelPayedReservation($body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#cancelPayedReservation
     */
    public function cancelPayedReservation(RequestOrder $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YagaOrder\Server\Model\RequestOrder**](../Model/RequestOrder.md)| (*) информация о заказе, которая передается в методы работы с заказом. |

### Return type

[**YagaOrder\Server\Model\OrderStatusResult**](../Model/OrderStatusResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **checkPromocode**
> YagaOrder\Server\Model\CheckPromocodeResult checkPromocode($body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#checkPromocode
     */
    public function checkPromocode(RequestOrder $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YagaOrder\Server\Model\RequestOrder**](../Model/RequestOrder.md)| (*) информация о заказе, которая передается в методы работы с заказом. |

### Return type

[**YagaOrder\Server\Model\CheckPromocodeResult**](../Model/CheckPromocodeResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **clearReservation**
> YagaOrder\Server\Model\OrderStatusResult clearReservation($body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#clearReservation
     */
    public function clearReservation(RequestOrder $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YagaOrder\Server\Model\RequestOrder**](../Model/RequestOrder.md)| (*) информация о заказе, которая передается в методы работы с заказом. |

### Return type

[**YagaOrder\Server\Model\OrderStatusResult**](../Model/OrderStatusResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getOrder**
> YagaOrder\Server\Model\Order getOrder($body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#getOrder
     */
    public function getOrder(RequestOrder $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YagaOrder\Server\Model\RequestOrder**](../Model/RequestOrder.md)| (*) информация о заказе, которая передается в методы работы с заказом. |

### Return type

[**YagaOrder\Server\Model\Order**](../Model/Order.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getOrderStatus**
> YagaOrder\Server\Model\OrderStatusResult getOrderStatus($body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#getOrderStatus
     */
    public function getOrderStatus(RequestOrder $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YagaOrder\Server\Model\RequestOrder**](../Model/RequestOrder.md)| (*) информация о заказе, которая передается в методы работы с заказом. |

### Return type

[**YagaOrder\Server\Model\OrderStatusResult**](../Model/OrderStatusResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getSessionSeats**
> YagaOrder\Server\Model\GetSessionSeatsResult getSessionSeats($sessionId, $venueId, $eventId, $hallId, $sessionTime, $additional)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#getSessionSeats
     */
    public function getSessionSeats($sessionId = null, $venueId = null, $eventId = null, $hallId = null, \DateTime $sessionTime = null, array $additional = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **string**|  | [optional]
 **venueId** | **string**|  | [optional]
 **eventId** | **string**|  | [optional]
 **hallId** | **string**|  | [optional]
 **sessionTime** | **\DateTime**|  | [optional]
 **additional** | [**string**](../Model/string.md)|  | [optional]

### Return type

[**YagaOrder\Server\Model\GetSessionSeatsResult**](../Model/GetSessionSeatsResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getTicketPdf**
> YagaOrder\Server\Model\GetTicketPdfResult getTicketPdf($ticketId, $body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#getTicketPdf
     */
    public function getTicketPdf($ticketId, RequestOrder $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **string**| (*) идентификатор билета |
 **body** | [**YagaOrder\Server\Model\RequestOrder**](../Model/RequestOrder.md)| (*) информация о заказе, которая передается в методы работы с заказом. |

### Return type

[**YagaOrder\Server\Model\GetTicketPdfResult**](../Model/GetTicketPdfResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **refundNotify**
> YagaOrder\Server\Model\OrderStatusResult refundNotify($body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#refundNotify
     */
    public function refundNotify(RefundNotify $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YagaOrder\Server\Model\RefundNotify**](../Model/RefundNotify.md)|  |

### Return type

[**YagaOrder\Server\Model\OrderStatusResult**](../Model/OrderStatusResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **reserveTickets**
> YagaOrder\Server\Model\ReserveTicketsResult reserveTickets($body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#reserveTickets
     */
    public function reserveTickets(RequestOrder $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YagaOrder\Server\Model\RequestOrder**](../Model/RequestOrder.md)| (*) информация о заказе, которая передается в методы работы с заказом. |

### Return type

[**YagaOrder\Server\Model\ReserveTicketsResult**](../Model/ReserveTicketsResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **saleApprove**
> YagaOrder\Server\Model\OrderStatusResult saleApprove($body)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaOrderApiInterface.php

namespace Acme\MyBundle\Api;

use YagaOrder\Server\Api\YagaOrderApiInterface;

class YagaOrderApi implements YagaOrderApiInterface
{

    // ...

    /**
     * Implementation of YagaOrderApiInterface#saleApprove
     */
    public function saleApprove(RequestOrder $body)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YagaOrder\Server\Model\RequestOrder**](../Model/RequestOrder.md)| (*) информация о заказе, которая передается в методы работы с заказом. |

### Return type

[**YagaOrder\Server\Model\OrderStatusResult**](../Model/OrderStatusResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

